# Homebrew Tap for ClickHouse

This Tap is based on [https://github.com/Altinity/homebrew-clickhouse](altinity/clickhouse), removing the bits we don't need.

## Available formulae

```text
clickhouse                       - alias, same as clickhouse@stable
clickhouse-client                - alias, same as clickhouse@stable
clickhouse-server                - alias, same as clickhouse@stable

clickhouse@stable                - alias, always points to the latest stable versioned formula (clickhouse@22.3)
clickhouse@lts                   - alias, always points to the latest LTS versioned formula (clickhouse@22.3)

clickhouse@22.3                  - the latest version
clickhouse@22.2                  - keg-only
```

## Quick start

```sh
brew tap brodock/clickhouse https://gitlab.com/brodock/homebrew-clickhouse
brew install clickhouse@stable
```

Note, that the installation doesn't require `sudo` and will deploy ClickHouse under the standard Homebrew prefix.

## Running ClickHouse server

Do not use `sudo` ever. Do not start the ClickHouse server manually, instead use `brew services`:

```sh
brew services start clickhouse
# ..or
brew services start clickhouse@21.11
# ...and so on.
```

ClickHouse is deployed under the standard Homebrew prefix. The relevant directories are:

```text
Config:    $(brew --prefix)/etc/clickhouse-server/
Data:      $(brew --prefix)/var/lib/clickhouse/
Logs:      $(brew --prefix)/var/log/clickhouse-server/
PID file:  $(brew --prefix)/var/run/clickhouse-server/
```

These files and directories will be preserved between installations.

Make sure you stop the server, when upgrading the formula.

If you absolutely need to run ClickHouse server manually, the command that corresponds to `brew services start clickhouse` would be:

```sh
$(brew --prefix clickhouse)/bin/clickhouse server --config-file $(brew --prefix)/etc/clickhouse-server/config.xml --pid-file $(brew --prefix)/var/run/clickhouse-server/clickhouse-server.pid
```

## Versioned formulae

All except the latest versioned ClickHouse formulae are configured as [keg-only](https://docs.brew.sh/FAQ#what-does-keg-only-mean), so in order to refer to an executable from such formula you have to provide the full path to it, e.g.:

```sh
$(brew --prefix clickhouse@21.11)/bin/clickhouse client
```

## Pre-built binary packages (bottles)

Bottles for the following platforms are available:

```text
macOS Monterey (version 12) on Intel
macOS Monterey (version 12) on Apple silicon
```

## Building (the latest versions) from sources

Formulae will be built from sources automatically if the corresponding bottles are not available for your platform.

If you want to see the progress, add `--verbose`:

```sh
brew install --verbose clickhouse
```

You can also build the latest version (`HEAD`) of the sources for a formula:

```sh
brew install --HEAD --verbose clickhouse
```

The above command will check out the tip of the branch that corresponds to that specific version (e.g., branch [21.11](https://github.com/ClickHouse/ClickHouse/tree/21.11) for `clickhouse@21.11` and so on) and build it from sources.

## Homebrew on Linux (Linuxbrew)

Building the formulae from this tap is not tested in Linux, and bottles are not available,
but there are no known conceptual problems, and they should generally work.
